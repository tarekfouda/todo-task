part of 'router_imports.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  final List<AutoRoute> routes = [
    AdaptiveRoute(page: Splash.page, initial: true),
    AdaptiveRoute(page: Login.page),
    AutoRoute(page: HomeRoute.page),
  ];
}
