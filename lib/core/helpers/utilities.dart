import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../features/auth/data/models/user_model/user_model.dart';
import '../../features/auth/presentation/manager/user_cubit/user_cubit.dart';
import '../constants/app_constants.dart';
import 'di.dart';
import 'global_state.dart';

@lazySingleton
class Utilities {
  Future<void> saveUserData(BuildContext context, UserModel model) async {
    getIt<SharedPreferences>().setString(ApplicationConstants.userEncoded, json.encode(model.toJson()));
    GlobalState.instance.set(ApplicationConstants.keyToken, model.token);
    context.read<UserCubit>().onUpdateUserData(model);
    log("saveUserData =====> ${model.toJson()}");
  }

  String convertDigitsToLatin(String s) {
    var sb = StringBuffer();
    for (int i = 0; i < s.length; i++) {
      switch (s[i]) {
        //Arabic digits
        case '\u0660':
          sb.write('0');
          break;
        case '\u0661':
          sb.write('1');
          break;
        case '\u0662':
          sb.write('2');
          break;
        case '\u0663':
          sb.write('3');
          break;
        case '\u0664':
          sb.write('4');
          break;
        case '\u0665':
          sb.write('5');
          break;
        case '\u0666':
          sb.write('6');
          break;
        case '\u0667':
          sb.write('7');
          break;
        case '\u0668':
          sb.write('8');
          break;
        case '\u0669':
          sb.write('9');
          break;
        default:
          sb.write(s[i]);
          break;
      }
    }
    return sb.toString();
  }
}
