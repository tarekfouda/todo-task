import 'package:flutter_tdd/core/constants/app_config.dart';

class ApiNames{
  static String baseUrl = AppConfig.instance.baseUrl;

  // auth routes
  static const String login = "auth/login";
  static const String refreshToken = "auth";
  static const String todos = "todos";
  static const String deleteTodo = "todos/";
  static const String editTodo = "todos/";
  static const String addTodo = "todos/add";





}