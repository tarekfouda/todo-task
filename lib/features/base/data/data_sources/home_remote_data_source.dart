import 'package:flutter_tdd/core/http/models/result.dart';

import '../../domain/entities/edit_task_entity.dart';
import '../../domain/entities/task_entity.dart';
import '../../domain/entities/todos_entity.dart';
import '../models/paginated_todo_model/paginated_todo_model.dart';
import '../models/paginated_todo_model/todo_model.dart';

abstract class HomeRemoteDataSource {

  Future<MyResult<PaginatedTodoModel>> getTodosList(TodosEntity param);
  Future<MyResult<TodoModel>> deleteTodoTask(int params);
  Future<MyResult<TodoModel>> addTodoTask(TaskEntity params);
  Future<MyResult<TodoModel>> editTodoTask(EditTaskEntity params);
}