import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/core/http/models/result.dart';
import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/paginated_todo_model.dart';
import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/todo_model.dart';
import 'package:injectable/injectable.dart';

import '../../domain/entities/edit_task_entity.dart';
import '../../domain/entities/task_entity.dart';
import '../../domain/entities/todos_entity.dart';
import 'home_remote_data_source.dart';

@Injectable(as: HomeRemoteDataSource)
class ImplHomeRemoteDataSource extends HomeRemoteDataSource {
  @override
  Future<MyResult<PaginatedTodoModel>> getTodosList(TodosEntity param) async {
    String headers = '?limit=${param.limit}&skip=${param.skip}';
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.todos + headers,
      requestMethod: RequestMethod.get,
      responseType: ResType.model,
      refresh: param.refresh??false,
      toJsonFunc: (json) => PaginatedTodoModel.fromJson(json),
    );
    return await GenericHttpImpl<PaginatedTodoModel>()(model);
  }

  @override
  Future<MyResult<TodoModel>> deleteTodoTask(int params) async {
    HttpRequestModel model = HttpRequestModel(
        url: "${ApiNames.deleteTodo}$params",
        requestMethod: RequestMethod.delete,
        showLoader: true,
        responseType: ResType.model,
        errorFunc: (data) => data,
        toJsonFunc: (json) => TodoModel.fromJson(json));
    return await GenericHttpImpl<TodoModel>()(model);
  }
  @override
  Future<MyResult<TodoModel>> addTodoTask(TaskEntity params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.addTodo,
        requestMethod: RequestMethod.post,
        showLoader: false,
        requestBody: params.toJson(),
        responseType: ResType.model,
        errorFunc: (data) => data,
        toJsonFunc: (json) => TodoModel.fromJson(json));
    return await GenericHttpImpl<TodoModel>()(model);
  }
  @override
  Future<MyResult<TodoModel>> editTodoTask(EditTaskEntity params) async {
    HttpRequestModel model = HttpRequestModel(
        url: "${ApiNames.editTodo}${params.id}",
        requestMethod: RequestMethod.put,
        showLoader: true,
        requestBody: params.toJson(),
        responseType: ResType.model,
        errorFunc: (data) => data,
        toJsonFunc: (json) => TodoModel.fromJson(json));
    return await GenericHttpImpl<TodoModel>()(model);
  }
}
