// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'paginated_todo_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PaginatedTodoModel _$PaginatedTodoModelFromJson(Map<String, dynamic> json) {
  return _PaginatedTodoModel.fromJson(json);
}

/// @nodoc
mixin _$PaginatedTodoModel {
  @JsonKey(name: "todos")
  List<TodoModel> get todos => throw _privateConstructorUsedError;
  @JsonKey(name: "total", nullable: true, defaultValue: 0)
  int get total => throw _privateConstructorUsedError;
  @JsonKey(name: "skip", nullable: true, defaultValue: 0)
  int get skip => throw _privateConstructorUsedError;
  @JsonKey(name: "limit", nullable: true, defaultValue: 0)
  int get limit => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginatedTodoModelCopyWith<PaginatedTodoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginatedTodoModelCopyWith<$Res> {
  factory $PaginatedTodoModelCopyWith(
          PaginatedTodoModel value, $Res Function(PaginatedTodoModel) then) =
      _$PaginatedTodoModelCopyWithImpl<$Res, PaginatedTodoModel>;
  @useResult
  $Res call(
      {@JsonKey(name: "todos") List<TodoModel> todos,
      @JsonKey(name: "total", nullable: true, defaultValue: 0) int total,
      @JsonKey(name: "skip", nullable: true, defaultValue: 0) int skip,
      @JsonKey(name: "limit", nullable: true, defaultValue: 0) int limit});
}

/// @nodoc
class _$PaginatedTodoModelCopyWithImpl<$Res, $Val extends PaginatedTodoModel>
    implements $PaginatedTodoModelCopyWith<$Res> {
  _$PaginatedTodoModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todos = null,
    Object? total = null,
    Object? skip = null,
    Object? limit = null,
  }) {
    return _then(_value.copyWith(
      todos: null == todos
          ? _value.todos
          : todos // ignore: cast_nullable_to_non_nullable
              as List<TodoModel>,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int,
      skip: null == skip
          ? _value.skip
          : skip // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PaginatedTodoModelImplCopyWith<$Res>
    implements $PaginatedTodoModelCopyWith<$Res> {
  factory _$$PaginatedTodoModelImplCopyWith(_$PaginatedTodoModelImpl value,
          $Res Function(_$PaginatedTodoModelImpl) then) =
      __$$PaginatedTodoModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "todos") List<TodoModel> todos,
      @JsonKey(name: "total", nullable: true, defaultValue: 0) int total,
      @JsonKey(name: "skip", nullable: true, defaultValue: 0) int skip,
      @JsonKey(name: "limit", nullable: true, defaultValue: 0) int limit});
}

/// @nodoc
class __$$PaginatedTodoModelImplCopyWithImpl<$Res>
    extends _$PaginatedTodoModelCopyWithImpl<$Res, _$PaginatedTodoModelImpl>
    implements _$$PaginatedTodoModelImplCopyWith<$Res> {
  __$$PaginatedTodoModelImplCopyWithImpl(_$PaginatedTodoModelImpl _value,
      $Res Function(_$PaginatedTodoModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todos = null,
    Object? total = null,
    Object? skip = null,
    Object? limit = null,
  }) {
    return _then(_$PaginatedTodoModelImpl(
      todos: null == todos
          ? _value._todos
          : todos // ignore: cast_nullable_to_non_nullable
              as List<TodoModel>,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int,
      skip: null == skip
          ? _value.skip
          : skip // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$PaginatedTodoModelImpl extends _PaginatedTodoModel {
  const _$PaginatedTodoModelImpl(
      {@JsonKey(name: "todos") required final List<TodoModel> todos,
      @JsonKey(name: "total", nullable: true, defaultValue: 0)
      required this.total,
      @JsonKey(name: "skip", nullable: true, defaultValue: 0)
      required this.skip,
      @JsonKey(name: "limit", nullable: true, defaultValue: 0)
      required this.limit})
      : _todos = todos,
        super._();

  factory _$PaginatedTodoModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$PaginatedTodoModelImplFromJson(json);

  final List<TodoModel> _todos;
  @override
  @JsonKey(name: "todos")
  List<TodoModel> get todos {
    if (_todos is EqualUnmodifiableListView) return _todos;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_todos);
  }

  @override
  @JsonKey(name: "total", nullable: true, defaultValue: 0)
  final int total;
  @override
  @JsonKey(name: "skip", nullable: true, defaultValue: 0)
  final int skip;
  @override
  @JsonKey(name: "limit", nullable: true, defaultValue: 0)
  final int limit;

  @override
  String toString() {
    return 'PaginatedTodoModel(todos: $todos, total: $total, skip: $skip, limit: $limit)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PaginatedTodoModelImpl &&
            const DeepCollectionEquality().equals(other._todos, _todos) &&
            (identical(other.total, total) || other.total == total) &&
            (identical(other.skip, skip) || other.skip == skip) &&
            (identical(other.limit, limit) || other.limit == limit));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_todos), total, skip, limit);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PaginatedTodoModelImplCopyWith<_$PaginatedTodoModelImpl> get copyWith =>
      __$$PaginatedTodoModelImplCopyWithImpl<_$PaginatedTodoModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PaginatedTodoModelImplToJson(
      this,
    );
  }
}

abstract class _PaginatedTodoModel extends PaginatedTodoModel {
  const factory _PaginatedTodoModel(
      {@JsonKey(name: "todos") required final List<TodoModel> todos,
      @JsonKey(name: "total", nullable: true, defaultValue: 0)
      required final int total,
      @JsonKey(name: "skip", nullable: true, defaultValue: 0)
      required final int skip,
      @JsonKey(name: "limit", nullable: true, defaultValue: 0)
      required final int limit}) = _$PaginatedTodoModelImpl;
  const _PaginatedTodoModel._() : super._();

  factory _PaginatedTodoModel.fromJson(Map<String, dynamic> json) =
      _$PaginatedTodoModelImpl.fromJson;

  @override
  @JsonKey(name: "todos")
  List<TodoModel> get todos;
  @override
  @JsonKey(name: "total", nullable: true, defaultValue: 0)
  int get total;
  @override
  @JsonKey(name: "skip", nullable: true, defaultValue: 0)
  int get skip;
  @override
  @JsonKey(name: "limit", nullable: true, defaultValue: 0)
  int get limit;
  @override
  @JsonKey(ignore: true)
  _$$PaginatedTodoModelImplCopyWith<_$PaginatedTodoModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
