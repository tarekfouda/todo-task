// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'todo_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TodoModel _$TodoModelFromJson(Map<String, dynamic> json) {
  return _TodoModel.fromJson(json);
}

/// @nodoc
mixin _$TodoModel {
  @JsonKey(name: "id")
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: "todo", nullable: true, defaultValue: "")
  String get todo => throw _privateConstructorUsedError;
  @JsonKey(name: "completed", nullable: true, defaultValue: false)
  bool get completed => throw _privateConstructorUsedError;
  @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
  bool get isDeleted => throw _privateConstructorUsedError;
  @JsonKey(name: "userId", nullable: true, defaultValue: 0)
  int get userId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TodoModelCopyWith<TodoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoModelCopyWith<$Res> {
  factory $TodoModelCopyWith(TodoModel value, $Res Function(TodoModel) then) =
      _$TodoModelCopyWithImpl<$Res, TodoModel>;
  @useResult
  $Res call(
      {@JsonKey(name: "id") int id,
      @JsonKey(name: "todo", nullable: true, defaultValue: "") String todo,
      @JsonKey(name: "completed", nullable: true, defaultValue: false)
      bool completed,
      @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
      bool isDeleted,
      @JsonKey(name: "userId", nullable: true, defaultValue: 0) int userId});
}

/// @nodoc
class _$TodoModelCopyWithImpl<$Res, $Val extends TodoModel>
    implements $TodoModelCopyWith<$Res> {
  _$TodoModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? todo = null,
    Object? completed = null,
    Object? isDeleted = null,
    Object? userId = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      todo: null == todo
          ? _value.todo
          : todo // ignore: cast_nullable_to_non_nullable
              as String,
      completed: null == completed
          ? _value.completed
          : completed // ignore: cast_nullable_to_non_nullable
              as bool,
      isDeleted: null == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoModelImplCopyWith<$Res>
    implements $TodoModelCopyWith<$Res> {
  factory _$$TodoModelImplCopyWith(
          _$TodoModelImpl value, $Res Function(_$TodoModelImpl) then) =
      __$$TodoModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "id") int id,
      @JsonKey(name: "todo", nullable: true, defaultValue: "") String todo,
      @JsonKey(name: "completed", nullable: true, defaultValue: false)
      bool completed,
      @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
      bool isDeleted,
      @JsonKey(name: "userId", nullable: true, defaultValue: 0) int userId});
}

/// @nodoc
class __$$TodoModelImplCopyWithImpl<$Res>
    extends _$TodoModelCopyWithImpl<$Res, _$TodoModelImpl>
    implements _$$TodoModelImplCopyWith<$Res> {
  __$$TodoModelImplCopyWithImpl(
      _$TodoModelImpl _value, $Res Function(_$TodoModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? todo = null,
    Object? completed = null,
    Object? isDeleted = null,
    Object? userId = null,
  }) {
    return _then(_$TodoModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      todo: null == todo
          ? _value.todo
          : todo // ignore: cast_nullable_to_non_nullable
              as String,
      completed: null == completed
          ? _value.completed
          : completed // ignore: cast_nullable_to_non_nullable
              as bool,
      isDeleted: null == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$TodoModelImpl extends _TodoModel {
  const _$TodoModelImpl(
      {@JsonKey(name: "id") required this.id,
      @JsonKey(name: "todo", nullable: true, defaultValue: "")
      required this.todo,
      @JsonKey(name: "completed", nullable: true, defaultValue: false)
      required this.completed,
      @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
      required this.isDeleted,
      @JsonKey(name: "userId", nullable: true, defaultValue: 0)
      required this.userId})
      : super._();

  factory _$TodoModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$TodoModelImplFromJson(json);

  @override
  @JsonKey(name: "id")
  final int id;
  @override
  @JsonKey(name: "todo", nullable: true, defaultValue: "")
  final String todo;
  @override
  @JsonKey(name: "completed", nullable: true, defaultValue: false)
  final bool completed;
  @override
  @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
  final bool isDeleted;
  @override
  @JsonKey(name: "userId", nullable: true, defaultValue: 0)
  final int userId;

  @override
  String toString() {
    return 'TodoModel(id: $id, todo: $todo, completed: $completed, isDeleted: $isDeleted, userId: $userId)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.todo, todo) || other.todo == todo) &&
            (identical(other.completed, completed) ||
                other.completed == completed) &&
            (identical(other.isDeleted, isDeleted) ||
                other.isDeleted == isDeleted) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, todo, completed, isDeleted, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoModelImplCopyWith<_$TodoModelImpl> get copyWith =>
      __$$TodoModelImplCopyWithImpl<_$TodoModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TodoModelImplToJson(
      this,
    );
  }
}

abstract class _TodoModel extends TodoModel {
  const factory _TodoModel(
      {@JsonKey(name: "id") required final int id,
      @JsonKey(name: "todo", nullable: true, defaultValue: "")
      required final String todo,
      @JsonKey(name: "completed", nullable: true, defaultValue: false)
      required final bool completed,
      @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
      required final bool isDeleted,
      @JsonKey(name: "userId", nullable: true, defaultValue: 0)
      required final int userId}) = _$TodoModelImpl;
  const _TodoModel._() : super._();

  factory _TodoModel.fromJson(Map<String, dynamic> json) =
      _$TodoModelImpl.fromJson;

  @override
  @JsonKey(name: "id")
  int get id;
  @override
  @JsonKey(name: "todo", nullable: true, defaultValue: "")
  String get todo;
  @override
  @JsonKey(name: "completed", nullable: true, defaultValue: false)
  bool get completed;
  @override
  @JsonKey(name: "isDeleted", nullable: true, defaultValue: false)
  bool get isDeleted;
  @override
  @JsonKey(name: "userId", nullable: true, defaultValue: 0)
  int get userId;
  @override
  @JsonKey(ignore: true)
  _$$TodoModelImplCopyWith<_$TodoModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
