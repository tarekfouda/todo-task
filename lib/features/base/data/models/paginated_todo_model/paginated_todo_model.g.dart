// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paginated_todo_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PaginatedTodoModelImpl _$$PaginatedTodoModelImplFromJson(
        Map<String, dynamic> json) =>
    _$PaginatedTodoModelImpl(
      todos: (json['todos'] as List<dynamic>)
          .map((e) => TodoModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      total: json['total'] as int? ?? 0,
      skip: json['skip'] as int? ?? 0,
      limit: json['limit'] as int? ?? 0,
    );

Map<String, dynamic> _$$PaginatedTodoModelImplToJson(
        _$PaginatedTodoModelImpl instance) =>
    <String, dynamic>{
      'todos': instance.todos.map((e) => e.toJson()).toList(),
      'total': instance.total,
      'skip': instance.skip,
      'limit': instance.limit,
    };
