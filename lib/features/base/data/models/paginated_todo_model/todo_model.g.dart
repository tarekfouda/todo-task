// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$TodoModelImpl _$$TodoModelImplFromJson(Map<String, dynamic> json) =>
    _$TodoModelImpl(
      id: json['id'] as int,
      todo: json['todo'] as String? ?? '',
      completed: json['completed'] as bool? ?? false,
      isDeleted: json['isDeleted'] as bool? ?? false,
      userId: json['userId'] as int? ?? 0,
    );

Map<String, dynamic> _$$TodoModelImplToJson(_$TodoModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'todo': instance.todo,
      'completed': instance.completed,
      'isDeleted': instance.isDeleted,
      'userId': instance.userId,
    };
