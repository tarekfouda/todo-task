import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/todo_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'paginated_todo_model.freezed.dart';
part 'paginated_todo_model.g.dart';

@freezed
@immutable
class PaginatedTodoModel with _$PaginatedTodoModel{
  const PaginatedTodoModel._();
  @JsonSerializable(explicitToJson: true)
  const factory PaginatedTodoModel({
    @JsonKey(name: "todos") required List<TodoModel> todos,
    @JsonKey(name: "total",nullable: true,defaultValue: 0) required int total,
    @JsonKey(name: "skip",nullable: true,defaultValue: 0) required int skip,
    @JsonKey(name: "limit",nullable: true,defaultValue: 0) required int limit,

  }) = _PaginatedTodoModel;


  factory PaginatedTodoModel.fromJson(Map<String, dynamic> json) =>
      _$PaginatedTodoModelFromJson(json);
}
