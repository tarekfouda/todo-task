import 'package:freezed_annotation/freezed_annotation.dart';

part 'todo_model.freezed.dart';
part 'todo_model.g.dart';

@freezed
@immutable
class TodoModel with _$TodoModel{
  const TodoModel._();
  @JsonSerializable(explicitToJson: true)
  const factory TodoModel({
    @JsonKey(name: "id") required int id,
    @JsonKey(name: "todo",nullable: true,defaultValue: "") required String todo,
    @JsonKey(name: "completed",nullable: true,defaultValue: false) required bool completed,
    @JsonKey(name: "isDeleted",nullable: true,defaultValue: false) required bool isDeleted,
    @JsonKey(name: "userId",nullable: true,defaultValue: 0) required int userId,

  }) = _TodoModel;


  factory TodoModel.fromJson(Map<String, dynamic> json) =>
      _$TodoModelFromJson(json);
}