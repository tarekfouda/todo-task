import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/http/models/result.dart';
import 'package:flutter_tdd/features/base/data/data_sources/home_remote_data_source.dart';
import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/paginated_todo_model.dart';
import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/todo_model.dart';
import 'package:flutter_tdd/features/base/domain/repositories/base_repository.dart';
import 'package:injectable/injectable.dart';

import '../../domain/entities/edit_task_entity.dart';
import '../../domain/entities/task_entity.dart';
import '../../domain/entities/todos_entity.dart';


@Injectable(as: BaseRepository)
class ImplBaseRepository extends BaseRepository  {

  @override
  Future<MyResult<PaginatedTodoModel>> getTodosList(TodosEntity param) async{
    var result = await getIt.get<HomeRemoteDataSource>().getTodosList(param);
    return result;
  }

  @override
  Future<MyResult<TodoModel>> addTodoTask(TaskEntity params) async{
    var result = await getIt.get<HomeRemoteDataSource>().addTodoTask(params);
    return result;
  }

  @override
  Future<MyResult<TodoModel>> deleteTodoTask(int params) async{
    var result = await getIt.get<HomeRemoteDataSource>().deleteTodoTask(params);
    return result;
  }

  @override
  Future<MyResult<TodoModel>> editTodoTask(EditTaskEntity params) async{
    var result = await getIt.get<HomeRemoteDataSource>().editTodoTask(params);
    return result;
  }



}