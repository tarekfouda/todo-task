import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/constants/dimens.dart';
import 'package:flutter_tdd/core/http/models/result.dart';
import 'package:flutter_tdd/core/theme/colors/colors_extension.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/base/presentation/pages/home/widgets/home_widgets_imports.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../../../core/constants/CustomButtonAnimation.dart';
import '../../../../../core/helpers/di.dart';
import '../../../data/models/paginated_todo_model/todo_model.dart';
import '../../../domain/entities/edit_task_entity.dart';
import '../../../domain/entities/task_entity.dart';
import '../../../domain/entities/todos_entity.dart';
import '../../../domain/repositories/base_repository.dart';

part 'home.dart';
part 'home_controller.dart';