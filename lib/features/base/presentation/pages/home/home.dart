part of 'home_imports.dart';

@RoutePage(name: "HomeRoute")
class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final HomeController controller = HomeController();

  @override
  void initState() {
    controller.initHome();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        SystemNavigator.pop();
      },
      child: Scaffold(
        backgroundColor: context.colors.background,
        appBar: const DefaultAppBar(
          title: "TASK MANAGER APP",
          showBack: false,
        ),
        body: PagedListView<int, TodoModel>(
          pagingController: controller.pagingController,
          padding: const EdgeInsets.symmetric(horizontal: Dimens.dp10, vertical: Dimens.dp5),
          builderDelegate: PagedChildBuilderDelegate<TodoModel>(
            noItemsFoundIndicatorBuilder: (_) => const NoDataFoundWidget(),
            firstPageProgressIndicatorBuilder: (_) => const TaskItemLoadingWidget(),
            newPageProgressIndicatorBuilder: (_) => const NewPageLoadingWidget(),
            noMoreItemsIndicatorBuilder: (_) => const NoMoreItemsFoundWidgets(),
            itemBuilder: (context, item, index) => TaskItemWidgets(model: item, controller: controller),
          ),
        ),
        floatingActionButton: AddTaskButtonWidget(controller: controller),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
