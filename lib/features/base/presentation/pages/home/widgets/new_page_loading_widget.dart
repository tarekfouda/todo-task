part of'home_widgets_imports.dart';
class NewPageLoadingWidget extends StatelessWidget {
  const NewPageLoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 16.h, bottom: 16.h),
        child: CircularProgressIndicator(color: context.colors.white));
  }
}
