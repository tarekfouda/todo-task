part of'home_widgets_imports.dart';
class AddTaskButtonWidget extends StatelessWidget {
  final HomeController controller;
  const AddTaskButtonWidget({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: context.colors.primary,
      onPressed: () => controller.onClickAddTask(context,controller),
      child: Icon(Icons.add,color: context.colors.white,)
    );
  }
}
