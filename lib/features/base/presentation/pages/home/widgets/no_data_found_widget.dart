part of'home_widgets_imports.dart';
class NoDataFoundWidget extends StatelessWidget {
  const NoDataFoundWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "No todos found",
        style: AppTextStyle.s16_w500(color: context.colors.white),
      ),
    );
  }
}
