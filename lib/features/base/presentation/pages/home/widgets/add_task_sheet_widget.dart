part of 'home_widgets_imports.dart';

class AddTaskSheetWidget extends StatelessWidget {
  final HomeController controller ;
  const AddTaskSheetWidget({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
        color: context.colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(Dimens.dp20),
          topRight: Radius.circular(Dimens.dp20),
        ),
      ),
      child: Form(
        key: controller.formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GenericTextField(
              controller: controller.taskController,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              fieldTypes: FieldTypes.rich,
              max: 9,
              type: TextInputType.text,
              action: TextInputAction.done,
              validate: (value) => value?.validateEmpty(),
              label: "Add Task",
            ),
            LoadingButton(
              title: "Add",
              onTap: () => controller.addTask(context),
              color: context.colors.primary,
              textColor: context.colors.white,
              btnKey: controller.btnKey,
              margin: const EdgeInsets.only(top: Dimens.dp20, right: Dimens.dp20, left: Dimens.dp20),
              fontSize: 16,
              height: 45,
              width: 170,
            )
          ],
        ),
      ),
    );
  }
}
