part of'home_widgets_imports.dart';
class NoMoreItemsFoundWidgets extends StatelessWidget {
  const NoMoreItemsFoundWidgets({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 16.h, bottom: 16.h),
      child: Text(
        "No more todos",
        style: AppTextStyle.s16_w500(color: context.colors.white),
      ),
    );
  }
}
