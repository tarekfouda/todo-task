import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_tdd/core/constants/dimens.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/theme/colors/colors_extension.dart';
import 'package:flutter_tdd/features/base/data/models/paginated_todo_model/todo_model.dart';
import 'package:flutter_tdd/features/base/presentation/pages/home/home_imports.dart';

import '../../../../../../core/constants/gaps.dart';
import '../../../../../../core/theme/text/app_text_style.dart';
import '../../../../../../core/widgets/GenericTextField.dart';
import '../../../../../../core/widgets/LoadingButton.dart';
import '../../../../../../core/widgets/shimmers/base_shimmer_widget.dart';

part"add_task_button_widget.dart";
part"add_task_sheet_widget.dart";
part 'new_page_loading_widget.dart';
part 'no_data_found_widget.dart';
part 'no_more_items_found_widget.dart';
part 'task_item_loading_widget.dart';
part 'task_item_widget.dart';