part of'home_widgets_imports.dart';
class TaskItemLoadingWidget extends StatelessWidget {
  const TaskItemLoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children:List.generate(10, (index) => Container(
        margin: const EdgeInsets.only(top: Dimens.dp14),
        child: BaseShimmerWidget(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(
              bottom: Dimens.dp10,
              left: Dimens.dp10,
            ),
            decoration: BoxDecoration(
              color: context.colors.white,
              borderRadius: BorderRadius.circular(Dimens.dp10),
            ),
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: Dimens.dp10,
                    right: Dimens.dp10,
                    left: Dimens.dp10,
                    bottom: Dimens.dp10,
                  ),
                  child: BaseShimmerWidget(
                    child: Container(
                      width: double.infinity,
                      height: Dimens.dp16,
                      color: context.colors.greyWhite,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ))
    );
  }
}
