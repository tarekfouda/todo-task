part of 'home_widgets_imports.dart';

class TaskItemWidgets extends StatelessWidget {
  final TodoModel model;
  final HomeController controller ;
  const TaskItemWidgets({super.key, required this.model, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: Dimens.dp14),
      child: Visibility(
        visible: !model.isDeleted,
        replacement:Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(
              bottom: Dimens.dp10,
              left: Dimens.dp10,
            ),
            decoration: BoxDecoration(
              color: context.colors.white.withOpacity(0.5),
              borderRadius: BorderRadius.circular(Dimens.dp10),
            ),
            child: Stack(
              children: [
                Padding(
                  padding:  EdgeInsets.only(
                    top: model.completed?Dimens.dp16:Dimens.dp10,
                    right: Dimens.dp10,
                    left: Dimens.dp10,
                    bottom: model.completed?Dimens.dp10:0,
                  ),
                  child:  Text(
                    model.todo,
                    style: AppTextStyle.s14_w400(color: context.colors.black.withOpacity(.5)).copyWith(height: 1.5),
                  ),
                ),
                Gaps.hGap5,
                Visibility(
                  visible: model.completed,
                  child: Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadiusDirectional.only(
                          topEnd: Radius.circular(Dimens.dp10),
                          bottomStart: Radius.circular(Dimens.dp10),
                        ),
                        color: context.colors.green.withOpacity(0.2),
                      ),
                      padding: const EdgeInsets.only(
                          top: 2, bottom: 2, left: 8, right: 8)
                          .w,
                      margin: const EdgeInsets.only(left: Dimens.dp10),
                      child: Text(
                        "Completed",
                        style:  AppTextStyle.s9_w500(
                            color: context.colors.green.withOpacity(.5))
                            .copyWith(height: 1.3),
                      ),
                    ),
                  ),
                ),
              ],
            )

        ) ,
        child: Slidable(
          endActionPane: ActionPane(
            closeThreshold: 0.5,
              motion: const ScrollMotion(), children: [
            SlidableAction(
              flex:  model.completed?2:1,
              label: model.completed?'Completed':null,
              onPressed: (con) => controller.editTask(model),
              backgroundColor: !model.completed?context.colors.primary:context.colors.green,
              icon: !model.completed?Icons.circle_outlined:Icons.check_circle,
            ),
            SlidableAction(
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(Dimens.dp10),
              bottomRight:   Radius.circular(Dimens.dp10)
              ),
              onPressed: (con) =>controller.deleteTask(model.id),
              backgroundColor: context.colors.red,
              icon: Icons.delete,
              flex: 1,
            ),
          ]),
          child: Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(
              bottom: Dimens.dp10,
              left: Dimens.dp10,
            ),
            decoration: BoxDecoration(
              color: context.colors.white,
              borderRadius: BorderRadius.circular(Dimens.dp10),
            ),
            child: Stack(
              children: [
                Padding(
                  padding:  EdgeInsets.only(
                    top: model.completed?Dimens.dp16:Dimens.dp10,
                    right: Dimens.dp10,
                    left: Dimens.dp10,
                    bottom: model.completed?Dimens.dp10:0,
                  ),
                  child:  Text(
                    model.todo,
                    style: AppTextStyle.s14_w400(color: context.colors.black).copyWith(height: 1.5),
                  ),
                ),
                Gaps.hGap5,
                Visibility(
                  visible: model.completed,
                  child: Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadiusDirectional.only(
                          topEnd: Radius.circular(Dimens.dp10),
                          bottomStart: Radius.circular(Dimens.dp10),
                        ),
                        color: context.colors.green.withOpacity(0.2),
                      ),
                      padding: const EdgeInsets.only(
                          top: 2, bottom: 2, left: 8, right: 8)
                          .w,
                      margin: const EdgeInsets.only(left: Dimens.dp10),
                      child: Text(
                        "Completed",
                        style:  AppTextStyle.s9_w500(
                            color: context.colors.green)
                            .copyWith(height: 1.3),
                      ),
                    ),
                  ),
                ),
              ],
            )

          ),
        ),
      ),
    );
  }
}
