part of 'home_imports.dart';

class HomeController {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  TextEditingController taskController = TextEditingController();
  int pageSize = 20;
  final PagingController<int, TodoModel> pagingController = PagingController(firstPageKey: 1);

  Future<void> _getTodosListWithPagination({bool refresh = true, int currentPage = 1}) async {
    var params = _todosEntity(currentPage, refresh);

    try {
      final pages = await getIt.get<BaseRepository>().getTodosList(params);
      var todosList = pages.data?.todos;
      final isLastPage = todosList!.length < pageSize;

      if (pages.data?.skip == 1) {
        pagingController.itemList = [];
      }

      if (isLastPage) {
        pagingController.appendLastPage(todosList);
      } else {
        final nextPageKey = currentPage + 1;
        pagingController.appendPage(todosList.toList(), nextPageKey);
      }
    } catch (e) {
      pagingController.error = e;
    }
  }

  void initHome() {
    _getTodosListWithPagination(refresh: false);
    pagingController.addPageRequestListener((pageKey) {
      _getTodosListWithPagination(currentPage: pageKey);
    });
  }

  void onClickAddTask(BuildContext context, HomeController controller) {
    showModalBottomSheet(
      backgroundColor: context.colors.white,
      context: context,
      builder: (context) {
        return AddTaskSheetWidget(controller: controller);
      },
    );
  }

  Future<void> deleteTask(int todoId) async {
    var result = await getIt.get<BaseRepository>().deleteTodoTask(todoId);
    if (result.data != null) {
      pagingController.refresh();
    }
  }

  Future<void> addTask(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      btnKey.currentState!.animateForward();
      var userId = context.read<UserCubit>().state.model?.id;
      TaskEntity task = _taskEntity(userId);
      var result = await getIt.get<BaseRepository>().addTodoTask(task);
      if (context.mounted) {
        _handleSuccessAddTask(result, context);
        btnKey.currentState!.animateReverse();
      }
    }
  }

  Future<void> editTask(TodoModel model) async {
    EditTaskEntity task = EditTaskEntity(id: model.id, completed: model.completed ? false : true);
    var result = await getIt.get<BaseRepository>().editTodoTask(task);
    if (result.data != null) {
      pagingController.refresh();
    }
  }

  void _handleSuccessAddTask(MyResult<TodoModel> result, BuildContext context) {
    if (result.data != null) {
      if (context.mounted) {
        taskController.clear();
        btnKey.currentState!.animateReverse();
        AutoRouter.of(context).pop();
        pagingController.refresh();
      }
    }
  }

  TaskEntity _taskEntity(int? userId) => TaskEntity(completed: false, todo: taskController.text, userId: userId!);

  TodosEntity _todosEntity(int currentPage, bool refresh) {
    return TodosEntity(
      limit: pageSize,
      skip: currentPage * 10,
      refresh: refresh,
    );
  }

  void dispose() {
    pagingController.dispose();
  }
}
