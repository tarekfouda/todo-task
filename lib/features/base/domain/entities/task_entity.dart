class TaskEntity{
  final int userId;
  final String todo;
  final bool completed;
  TaskEntity({required this.userId, required this.todo, required this.completed});
  Map<String, dynamic>toJson(){
    return {
      'userId': userId,
      'todo': todo,
      'completed': completed,
    };
  }

}
