class EditTaskEntity {
  final int id;
  final bool completed;

  EditTaskEntity({required this.id, required this.completed});

  Map<String, dynamic> toJson() {
    return {
      'completed': completed,
    };
  }
}
