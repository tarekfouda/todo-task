class TodosEntity {
  final int limit;
  final int skip;
  bool? refresh;

  TodosEntity({
    required this.limit,
    required this.skip,
    this.refresh ,
  });

  factory TodosEntity.fromJson(Map<String, dynamic> json) {
    return TodosEntity(
      limit: json['limit'],
      skip: json['skip'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'limit': limit,
      'skip': skip,
    };
  }
}
