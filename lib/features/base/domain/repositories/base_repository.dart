import 'package:flutter_tdd/core/http/models/result.dart';

import '../../data/models/paginated_todo_model/paginated_todo_model.dart';
import '../../data/models/paginated_todo_model/todo_model.dart';
import '../entities/edit_task_entity.dart';
import '../entities/task_entity.dart';
import '../entities/todos_entity.dart';

abstract class BaseRepository {

  Future<MyResult<PaginatedTodoModel>> getTodosList(TodosEntity param);
  Future<MyResult<TodoModel>> deleteTodoTask(int params);
  Future<MyResult<TodoModel>> addTodoTask(TaskEntity params);
  Future<MyResult<TodoModel>> editTodoTask(EditTaskEntity params);
}