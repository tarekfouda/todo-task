import 'package:injectable/injectable.dart';

import '../../../../core/http/generic_http/api_names.dart';
import '../../../../core/http/generic_http/generic_http.dart';
import '../../../../core/http/models/http_request_model.dart';
import '../../../../core/http/models/result.dart';
import '../../domain/entities/login_entity.dart';
import '../models/user_model/user_model.dart';
import 'auth_data_source.dart';

@Injectable(as: AuthDataSource)
class ImplAuthDataSource extends AuthDataSource {
  Future<MyResult <UserModel>> userLogin(LoginEntity params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.login,
        requestBody: params.toJson(),
        requestMethod: RequestMethod.post,
        showLoader: false,
        responseType: ResType.model,
        errorFunc: (data) => data,
        toJsonFunc: (json)=> UserModel.fromJson(json)
    );
    return await GenericHttpImpl<UserModel>()(model);
  }

}
