
import '../../../../core/http/models/result.dart';
import '../../domain/entities/login_entity.dart';
import '../models/user_model/user_model.dart';

abstract class AuthDataSource {
  Future<MyResult <UserModel>> userLogin(LoginEntity params);
}
