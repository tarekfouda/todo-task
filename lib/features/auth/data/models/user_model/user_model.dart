import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
@immutable
class UserModel with _$UserModel {
  const UserModel._();

  @JsonSerializable(explicitToJson: true)
  const factory UserModel({
    @JsonKey(name: "id") required int id,
    @JsonKey(name: "username",nullable: true,defaultValue: "") required String username,
    @JsonKey(name: "firstName",nullable: true,defaultValue: "") required String firstName,
    @JsonKey(name: "lastName",nullable: true,defaultValue: "") required String lastName,
    @JsonKey(name: "email",nullable: true,defaultValue: "") required String email,
    @JsonKey(name: "phone",nullable: true,defaultValue: "") required String phone,
    @JsonKey(name: "gender",nullable: true,defaultValue: "") required String gender,
    @JsonKey(name: "image",nullable: true,defaultValue: "") required String image,
    @JsonKey(name: "token",nullable: true,defaultValue: "") required String token,
    @JsonKey(name: "refreshToken",nullable: true,defaultValue: "") required String refreshToken,
  }) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
}
