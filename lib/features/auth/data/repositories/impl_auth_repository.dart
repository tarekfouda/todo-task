
import 'package:flutter_tdd/core/http/models/result.dart';
import 'package:flutter_tdd/features/auth/data/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/login_entity.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/helpers/di.dart';
import '../../domain/repositories/auth_repository.dart';
import '../data_source/auth_data_source.dart';


@Injectable(as: AuthRepository)
class ImplAuthRepository extends AuthRepository {
  @override
  Future<MyResult<UserModel>> userLogin(LoginEntity params) async{
    var result = await getIt.get<AuthDataSource>().userLogin(params);
    return result;
  }

  }
