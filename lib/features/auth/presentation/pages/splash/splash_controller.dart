part of 'splash_imports.dart';

class SplashController {
  void manipulateSaveData(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var strUser = prefs.get(ApplicationConstants.userEncoded);
    if (strUser != null) {
      if (context.mounted) {
        UserModel user = UserModel.fromJson(json.decode("$strUser"));
        GlobalState.instance.set(ApplicationConstants.keyToken, user.token);
        context.read<UserCubit>().onUpdateUserData(user);
        await Future.delayed(const Duration(seconds: 3), () {});
        AutoRouter.of(context).push(const HomeRoute());
      }
    } else {
      await Future.delayed(const Duration(seconds: 3), () {});
      if (context.mounted) {
        AutoRouter.of(context).push(const Login());
      }
    }
  }
}
