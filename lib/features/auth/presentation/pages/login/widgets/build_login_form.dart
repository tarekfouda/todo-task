part of 'login_widgets_imports.dart';

class BuildLoginForm extends StatelessWidget {
  final LoginController controller;

  const BuildLoginForm({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: controller.formKey,
      child: Column(
        children: [
          GenericTextField(
            controller: controller.nameController,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateName(),
            label: "Name",
            margin: const EdgeInsets.only(top: 20),
            prefixIcon:  Padding(
              padding:  const EdgeInsets.all(10.0),
              child: Icon(
                Icons.person_outlined,
                color: context.colors.black,),
            ),
          ),
          GenericTextField(
            controller: controller.passwordController,
            fieldTypes: FieldTypes.password,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.validatePassword(),
            label: "Password",
            margin: const EdgeInsets.only(top: 20),
            prefixIcon: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(Res.password),
            ),
          ),
        ],
      ),
    );
  }
}
