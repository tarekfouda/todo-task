part of 'login_imports.dart';

class LoginController {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future<void> onUserLogin(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      btnKey.currentState!.animateForward();
      LoginEntity params = _loginEntity();
      var results = await getIt<AuthRepository>().userLogin(params);
      if (results.data != null) {
        if (context.mounted) {
          context.read<DeviceCubit>().updateUserAuth(true);
          getIt<Utilities>().saveUserData(context, results.data!);
          getIt<SharedPreferences>().setString(ApplicationConstants.keyRefreshToken, results.data!.refreshToken);
          btnKey.currentState!.animateReverse();
          AutoRouter.of(context).push(const HomeRoute());
        }
      }
      btnKey.currentState!.animateReverse();
    }
  }

  LoginEntity _loginEntity() {
    return LoginEntity(name: nameController.text, password: passwordController.text);
  }
}
