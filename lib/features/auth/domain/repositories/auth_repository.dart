


import '../../../../core/http/models/result.dart';
import '../../data/models/user_model/user_model.dart';
import '../entities/login_entity.dart';

abstract class AuthRepository {
  Future<MyResult <UserModel>> userLogin(LoginEntity params);
}
