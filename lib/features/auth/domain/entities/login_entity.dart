class LoginEntity{
  final String name;
  final String password;

  LoginEntity({required this.name, required this.password});
  Map<String, dynamic> toJson() {
    return {
      'username': name,
      'password': password,
      'expiresInMins': 30,
    };
  }
}